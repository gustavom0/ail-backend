import Joi from 'joi';

export const evaluateRule = Joi.object({
  type: Joi.string()
    .required(),
  payload: Joi.object()
    .required(),
});
