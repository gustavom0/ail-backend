// Libraries
import { Router } from 'express';

// Controllers
import * as controller from './controller';

// Middlewares
import * as middlewares from '../../middlewares';

// Schema
import * as schema from '../../middlewares/schemas/ship';

// Router
const router = Router();

router.post(
  '/evaluate',
  middlewares.handlerValidation('evaluate', schema.evaluateRule),
  middlewares.handlerReponse(controller.executeEvaluate),
);

export default router;
