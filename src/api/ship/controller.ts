import { evaluate } from '../../runtime/evaluation';
import { Expression } from '../../lang/Expressions';

export const executeEvaluate = async (req: IReq) => {
  const payload: Expression = req.body as any;

  const result = evaluate(payload);

  if (typeof result !== 'undefined') {
    return result;
  }

  throw new Error("invalid arguments");
};
