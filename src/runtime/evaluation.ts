import { Expression } from "../lang/Expressions";
import { ExpressionType } from "../lang/ExpressionTypes";

// TODO: implement evaluate expression to accomplish tests
export function evaluate(expression: Expression): any {
  if (expression.type === ExpressionType.Const) {
    return expression.payload.value;
  }
  else if (expression.type === ExpressionType.Eq) {
    return evaluate(expression.payload.left) === evaluate(expression.payload.right);
  }
  else if (expression.type === ExpressionType.Or) {
    return expression.payload.expressions.reduce((a, b) => {
      const newA = typeof a === 'boolean' ? a : evaluate(a);
      return newA || evaluate(b);
    });
  }
  else if (expression.type === ExpressionType.And) {
    return expression.payload.expressions.reduce((a, b) => {
      const newA = typeof a === 'boolean' ? a : evaluate(a);
      return newA && evaluate(b);
    });
  }
  else if (expression.type === ExpressionType.StringToLower) {
    const value = evaluate(expression.payload.value) as string;
    return value.toLowerCase();
  }
  else if (expression.type === ExpressionType.StringToUpper) {
    const value = evaluate(expression.payload.value) as string;
    return value.toUpperCase();
  }
  else if (expression.type === ExpressionType.If) {
    return evaluate(expression.payload.test_expression) ? evaluate(expression.payload.if_true) : evaluate(expression.payload.if_false);
  }
  else if (expression.type === ExpressionType.Not) {
    return !evaluate(expression.payload.expression);
  }

  throw new Error("invalid type");
}
