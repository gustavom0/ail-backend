// Libraries
import { Application } from 'express';

// Components
import shipRoutes from './api/ship/route';

const routes = (app: Application) => {
  app.use('/ship', shipRoutes);
  app.get('/healthcheck', (_, res) => res.status(200).send('OK'));
};

export default routes;
